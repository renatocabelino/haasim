package br.ufes.inf.haasim.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.osgi.service.runtime.JadeRuntimeService;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class HAASimServiceActivator implements BundleActivator {

	private static BundleContext context;
	@SuppressWarnings("rawtypes")
	private static ServiceReference jadeRef;
	private static JadeRuntimeService jrs;
	private AgentController ac;
	private static float creationtime;
	private static int i, firstid, quantityagents, redial, lastExtension;
	private static String ipweb, iphost, agenttype;
	private static ArrayList<String> agents = new ArrayList<String>();
	private static PoissonDistribution pd;
	private static DatagramSocket clientSocket, serverSocket;
	private static Thread tSocket;
	private static jade.core.Runtime runtime;
	private static Profile profileContainer;
	private ContainerController ccHDashCloudGW;
	private int length = 10;
	private boolean useLetters = true;
	private boolean useNumbers = true;
	private String generatedString;

	/*
	 * Método interno para receber conexões via socket.
	 * 
	 * @param String - tipo de mensagem que será enviada por Socket.
	 */
	public void clientSocket(String tipo) {
		try {
			clientSocket = new DatagramSocket();
			InetAddress destino = InetAddress.getByName(iphost);
			String mensagem = null;
			int porta = 9992;
			switch (tipo) {
			case "hello":
				mensagem = "hello:";
				break;
			case "data":
				mensagem = "data:agents=" + quantityagents + "-redial=" + redial + "-frequency=" + creationtime;
				break;
			case "agent":
				mensagem = "agent:total=" + agents.size();
				break;
			}
			System.out.println(mensagem + " para o destino " + destino);
			byte[] dados = mensagem.getBytes();
			DatagramPacket pacote = new DatagramPacket(dados, dados.length, destino, porta);
			clientSocket.send(pacote);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Método externo para receber conexões via socket.
	 */
	public class ServerSocket implements Runnable {
		public void run() {
			int porta = 9992;
			DatagramPacket pacote = null;
			byte[] dados;
			try {
				System.out.println("Socket listening on " + InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				serverSocket = new DatagramSocket(porta);
				dados = new byte[100];
				pacote = new DatagramPacket(dados, dados.length);
				while (true) {
					serverSocket.receive(pacote);
					String mensagem = new String(pacote.getData(), 0, pacote.getLength());
					// System.out.println(mensagem);
					switch (mensagem.split(":")[0]) {
					case "agents":
						int qtt = Integer.parseInt((mensagem.split(":")[1]).split("=")[1]);
						if ((mensagem.split(":")[1]).split("=")[0].equals("start")) {
							if (agents.size() == 0)
								startAgents(qtt * quantityagents / 100);
							else
								startAgents(qtt * agents.size() / 100);
						}
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Método interno para iniciar mais agentes chamadores. param int - quantidade
	 * de agentes que serão iniciados.
	 */
	@SuppressWarnings("unchecked")
	private synchronized void startAgents(int qtt) {
		Object parametros[] = new Object[2];
		if (jadeRef != null) {
			jrs = context.getService(jadeRef);
			if (qtt >= 1) {
				try {
					if (agents.isEmpty()) {
						i = firstid;
						lastExtension = i + quantityagents;
					} else {
						i = lastExtension;
						lastExtension = i + qtt;
					}
					for (; i < lastExtension; i++) {
						generatedString = RandomStringUtils.random(length, useLetters, useNumbers).toUpperCase();
						parametros[0] = generatedString;
						parametros[1] = agenttype;
						agents.add(parametros[0].toString());
						try {
							Agent myAgent = new br.ufes.inf.haasim.logic.HAASimLogic();
							myAgent.setArguments(parametros);
							ac = ccHDashCloudGW.acceptNewAgent(parametros[0].toString(), myAgent);
							ac.start();
							Thread.sleep(pd.sample());
						} catch (StaleProxyException e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					System.out.println("Cannot start Agent: " + e);
				}
				clientSocket("agent");
			}
		} else {
			System.out.println("Cannot start Agent: JadeRuntimeService cannot be found");
		}
	}

	/*
	 * Método externo para ler o arquivo, e chamar o método para iniciar mais
	 * agentes chamadores. param BundleContext - contexto do bundle.
	 */
	public void start(BundleContext bundleContext) throws Exception {
		HAASimServiceActivator.context = bundleContext;
		jadeRef = context.getServiceReference(JadeRuntimeService.class.getName());
		// Get the JADE runtime interface (singleton)
		runtime = jade.core.Runtime.instance();
		// ac = jrs.createNewAgent("AgHAASimOrchestrator",
		// "br.ufes.inf.HAASim.logic.Orchestrator", null, "br.ufes.inf.HAASim.logic");
		// Create a Profile, where the launch arguments are stored
		profileContainer = new ProfileImpl();
		// criando container para agentes gateway
		profileContainer.setParameter(Profile.CONTAINER_NAME, "HDashCloud-GW");
		profileContainer.setParameter(Profile.MAIN_HOST, "localhost");
		profileContainer.setParameter(Profile.MAIN_PORT, "1099");
		profileContainer.setParameter(Profile.ACCEPT_FOREIGN_AGENTS, "true");
		ccHDashCloudGW = runtime.createAgentContainer(profileContainer);
		try {
			FileReader arq = new FileReader("/tmp/haasim.conf");
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine();
			while (linha != null) {
				if (!linha.startsWith("#")) {
					switch ((linha.split("="))[0]) {
					case ("quantityagents"):
						quantityagents = Integer.parseInt(linha.split("=")[1]);
						break;
					case ("creationtime"):
						creationtime = Float.parseFloat(linha.split("=")[1]);
						pd = new PoissonDistribution((double) (1000 / creationtime));
						break;
					case ("ipweb"):
						ipweb = linha.split("=")[1];
						break;
					case ("iphost"):
						iphost = linha.split("=")[1];
						break;

					case ("agenttype"):
						agenttype = linha.split("=")[1];
						break;
					}
				}
				linha = lerArq.readLine();
			}
			arq.close();
			clientSocket("data");
			clientSocket("hello");
			tSocket = new Thread(new ServerSocket());
			tSocket.start();
			startAgents(quantityagents);
		} catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
			System.exit(0);
		}
	}

	/*
	 * Método externo para desativar o agente, e remove-lo da lista. param String -
	 * nome do agente no formato nome.
	 */
	public void takeOff(String id) throws Exception {
		if (jadeRef != null) {
			ac = jrs.getAgent(id);
			System.out.println(ac.getName() + " offline");
			ac.kill();
			agents.remove(id);
		}
		clientSocket("agent");
	}

	/*
	 * Método externo para parar os agentes. param BundleContext - contexto do
	 * bundle.
	 */
	public synchronized void stop(BundleContext bundleContext) throws Exception {
		String agentName;
		if (jadeRef != null) {
			Iterator<String> it = agents.iterator();
			while (it.hasNext()) {
				agentName = it.next();
				ac = jrs.getAgent(agentName);
				System.out.println(ac.getName() + " offline");
				ac.kill();
				Thread.sleep(100);
			}
			agents.clear();
			context.ungetService(jadeRef);
		}
		tSocket.interrupt();
		clientSocket.close();
		serverSocket.disconnect();
		serverSocket.close(); // Dá erro, mas funciona
		HAASimServiceActivator.context = null;
	}

	public static String myIPAddress() {
		Enumeration<NetworkInterface> e = null;
		String myIP = null;
		try {
			e = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (e.hasMoreElements()) {
			NetworkInterface n = (NetworkInterface) e.nextElement();
			Enumeration<InetAddress> ee = n.getInetAddresses();
			while (ee.hasMoreElements()) {
				InetAddress i = (InetAddress) ee.nextElement();
				if (i.getHostAddress().contains("192"))
					myIP = i.getHostAddress();
			}
		}
		return myIP;
	}
}
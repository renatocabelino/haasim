package br.ufes.inf.haasim.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import jade.core.Agent;

public class HAASimLogic extends Agent {
	int nClassification = 0;
	private GeneratorProfiles gpHAASim = new GeneratorProfiles() {
	};
	private static final long serialVersionUID = 1L;
	private SchedulerFactory schedulerFactory = new StdSchedulerFactory();
	private Scheduler scheduler;
	private static int fBloodPressure=60000, fStep=60000, fHeartRate=60000, fTemperature = 60000;
	private static int fGlucose = 4320000;
	private static Random random = new Random();
	private static ArrayList<String> bloodpressureType = new ArrayList<String>(
			Arrays.asList("normal", "prehypertension", "hypertension1", "hypertension2"));
	private static ArrayList<String> glucoseType = new ArrayList<String>(
			Arrays.asList("normal", "impaired", "diabetes"));
	private static ArrayList<String> heartrateType = new ArrayList<String>(
			Arrays.asList("normal", "tachycardic", "bradycardic"));
	public String myAgentName;
	

	protected void setup() {
		try {
			scheduler = schedulerFactory.getScheduler();
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Object parametros[] = new Object[this.getArguments().length];
		parametros = this.getArguments();
		myAgentName = this.getLocalName();
		Vector<String> myGenerator = new Vector<String>(Arrays.asList(parametros[1].toString().split(":")));
		String myClassification = new String();
		Iterator<String> value = myGenerator.iterator();
		while (value.hasNext()) {
			String type_frequency = value.next().toString();
			final String type = type_frequency.split(",")[0];
			Integer frequency = 0;
			final String myTemplate = gpHAASim.getTemplate(type);

			switch (type) {
			case ("bloodpressure"):
				nClassification = random.nextInt(3);
				myClassification = bloodpressureType.get(nClassification);
				frequency = fBloodPressure;
				break;
			case ("glucose"):
				nClassification = random.nextInt(2);
				myClassification = glucoseType.get(nClassification);
				frequency = fGlucose;
				break;
			case ("heartrate"):
				nClassification = random.nextInt(2);
				myClassification = heartrateType.get(nClassification);
				frequency = fHeartRate;
				break;
			case ("step"):
				parametros[2] = "normal";
				frequency = fStep;
				break;
			case ("temperature"):
				parametros[2] = "normal";
				frequency = fTemperature;
				break;
			}

			JobDetail agentJob = JobBuilder.newJob(AgentJobs.class).withIdentity(myAgentName + "_" + type, myAgentName)
					.usingJobData("ProfileType", type).usingJobData("ProfileTemplate", myTemplate)
					.usingJobData("Classification", myClassification).usingJobData("AgentName", myAgentName).build();

			// criacao do trigger para execucao da geracao de valor em funcao dos tipos dos
			// geradores
			// Creating schedule time with trigger
			SimpleTriggerImpl agentTrigger = new SimpleTriggerImpl();
			agentTrigger.setStartTime(new Date(System.currentTimeMillis() + 500));
			agentTrigger.setRepeatCount(-1);
			agentTrigger.setRepeatInterval(frequency);
			agentTrigger.setName(myAgentName + "_" + type);
			agentTrigger.setJobGroup(type);
			// agentTrigger.setPriority(10); // Setting trigger 1 priority to 10
			try {
				scheduler.scheduleJob(agentJob, agentTrigger);
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				System.out.println("Erro ao escalonar job: " + e);
			}
		}
	}
}
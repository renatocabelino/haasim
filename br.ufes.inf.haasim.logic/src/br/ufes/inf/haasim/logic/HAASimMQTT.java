package br.ufes.inf.haasim.logic;

import java.util.UUID;
import java.util.concurrent.Callable;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

public class HAASimMQTT implements Callable<Void> {

	private IMqttClient client;
	private String genetatedData;
	private String publisherId = UUID.randomUUID().toString();
	private IMqttClient publisher;

	public HAASimMQTT(IMqttClient client, String cGeneratedData, String cPublisherID) {
		this.publisherId = cPublisherID;
        this.client = client;
        this.genetatedData = cGeneratedData;
        try {
			publisher = new MqttClient("tcp://172.16.16.49:1883",publisherId);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        try {
			publisher.connect(options);
		} catch (MqttSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Override
	public Void call() throws Exception {
	      if ( !client.isConnected()) {
	            return null;
	        }           
	        MqttMessage msg = readGeneratesData();
	        msg.setQos(0);
	        msg.setRetained(true);
	        client.publish("HAASim",msg);        
	        return null; 
	}
    private MqttMessage readGeneratesData() {        
        byte[] payload = this.genetatedData.getBytes();        
        return new MqttMessage(payload);           
    }

}

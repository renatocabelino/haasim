package br.ufes.inf.haasim.logic;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import jade.osgi.service.agentFactory.AgentFactoryService;

public class HAASimLogicActivator implements BundleActivator {

	private static BundleContext context;
	private AgentFactoryService agentFactory;
	private SchedulerFactory schedulerFactory;
	private Scheduler agentScheduler;
	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		HAASimLogicActivator.context = bundleContext;
		Bundle b = context.getBundle();
		agentFactory = new AgentFactoryService();
		agentFactory.init(b);
		try {
			//Properties profileScheduler = new Properties();
			//profileScheduler.put("org.quartz.threadPool.threadCount", quartzthreads);
			schedulerFactory = new StdSchedulerFactory();
			agentScheduler = schedulerFactory.getScheduler();
			agentScheduler.start();
			System.out.println("Quartz Scheduler started! ");
		} catch (SchedulerException e) {
			System.out.println("Erro ao iniciar Quartz Scheduler: " + e);
		}

	}

	public void stop(BundleContext bundleContext) throws Exception {
		agentFactory.clean();
		HAASimLogicActivator.context = null;
	}

}

package br.ufes.inf.haasim.logic;

import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AgentJobs implements Job {
	private GeneratorProfiles gpHAASim = new GeneratorProfiles() { };
	private MqttClient sampleClient;
	private String myAgentName;
	private String myClassification;
	private String myTemplate;
	private String myType;
	private JobDataMap dataMap;
	private Map<String, Object> myContext;
	public AgentJobs() throws MqttException {
        String[] broker       = (new String[] { "tcp://172.16.16.49:1883" });
        String mqttBroker = "tcp://172.16.16.49:1883" ;
        String clientId = this.toString();
        MemoryPersistence persistence = new MemoryPersistence();
        sampleClient = new MqttClient(mqttBroker.toString(), clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        connOpts.setConnectionTimeout(60);
        connOpts.setServerURIs(broker);
        if (!sampleClient.isConnected()) {
        	System.out.println("Connecting to broker: "+ mqttBroker);
        	sampleClient.connect(connOpts);
        	System.out.println("Connected");
        }
	}
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		dataMap = context.getJobDetail().getJobDataMap();
		myType = dataMap.getString("ProfileType");
		myTemplate = dataMap.getString("ProfileTemplate");
		myClassification = dataMap.getString("Classification");
		myAgentName = dataMap.getString("AgentName");
		myContext = gpHAASim.getContextData(myType, myClassification, myAgentName);
		String myProfileData = (gpHAASim.getData(myType, myTemplate, myContext));
		String content = new String(myProfileData);
        String topic        = myType;
        int qos             = 2;
        try {
            System.out.println("Publishing message: "+content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }
}

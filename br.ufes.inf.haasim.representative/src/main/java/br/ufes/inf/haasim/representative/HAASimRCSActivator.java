package br.ufes.inf.haasim.representative;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class HAASimRCSActivator implements BundleActivator {
	private int serverport;
	private int ncontainers=0;
	private String ipweb, lport;
	private static DatagramSocket clientSocket, serverSocket;
	private static Thread tSocket;
	private String myIPAddress;
	private ArrayList<String> cLocalPorts = new ArrayList<String>();
	private ArrayList<String> cPrivateIP = new ArrayList<String>();
	

	// The plug-in ID
	public static final String PLUGIN_ID = "br.ufes.inf.haasim.rcs"; //$NON-NLS-1$
	public void start(BundleContext context) throws Exception {
		cLocalPorts=getContainersLocalPort();
		System.out.println(cLocalPorts);
		myIPAddress = InetAddress.getLocalHost().getHostAddress();
		try {
			FileReader arq = new FileReader("haasimrcs.conf");
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine();
			while (linha != null) {
				if (!linha.startsWith("#")) {
					switch ((linha.split("="))[0]) {
					case ("serverport"):
						serverport = Integer.parseInt(linha.split("=")[1]);
						break;
					case ("ipweb"):
						ipweb = linha.split("=")[1];
						break;
					}
				}
				linha = lerArq.readLine();
			}
			arq.close();
		} catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
			System.exit(0);
		}
		clientSocket("host:" + myIPAddress, ipweb, serverport);
		tSocket = new Thread(new ServerSocket());
		tSocket.start();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		tSocket.interrupt();
		clientSocket.close();
		serverSocket.disconnect();
		serverSocket.close(); // Dá erro, mas funciona

	}

	public class ServerSocket implements Runnable {
		public void run() {
			String destination = new String();
			String srcIP = new String();
			String mensagem = new String();
			DatagramPacket pacote = null;
			byte[] dados;
			try {
				System.out.println("Socket listening on " + InetAddress.getLocalHost().getHostAddress().toString());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				serverSocket = new DatagramSocket(serverport);
				dados = new byte[100];
				pacote = new DatagramPacket(dados, dados.length);
				while (true) {
					serverSocket.receive(pacote);
					srcIP = pacote.getAddress().toString().split("/")[1];
					mensagem = new String(pacote.getData(), 0, pacote.getLength());
					System.out.println(mensagem);
					//messages from orchestrator to containers via RCS
					if (srcIP.equals(ipweb)) {
						switch (mensagem.split(":")[0]) {
						case "agents":
							cLocalPorts.clear();
							cLocalPorts=getContainersLocalPort();
							destination = (mensagem.split(":")[2].replace("=", ":"));
							lport = destination.split(":")[1];
							System.out.println(lport);
							destination = cPrivateIP.get(cLocalPorts.indexOf(lport));
							mensagem = mensagem.split(":")[0] + ":" + mensagem.split(":")[1];
							clientSocket(mensagem, destination, serverport );
							break;
							
						}
					} else {
						//messages from containers to orchestrator via RCS
						switch (mensagem.split(":")[0]) {
						case "hello":
							cLocalPorts.clear();
							cLocalPorts=getContainersLocalPort();
							mensagem = mensagem + InetAddress.getLocalHost().getHostAddress();
							destination = ipweb;
							cPrivateIP.add(srcIP);
							String nContainer = mensagem + "=" + cLocalPorts.get(ncontainers);
							ncontainers++;
							System.out.println("Total of active containers: " + ncontainers);
							clientSocket(nContainer, destination, serverport);
							System.out.println("enviando hello para o orquestrador: " + mensagem);
							break;
						default:
							mensagem = mensagem + ":" + InetAddress.getLocalHost().getHostAddress();
							destination = ipweb;
							clientSocket(mensagem, destination, serverport);
							break;
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void clientSocket(String cMessage, String cContainerIP, int cPort) {
		try {
			clientSocket = new DatagramSocket();
			InetAddress cDestination = InetAddress.getByName(cContainerIP);
			byte[] dados = cMessage.getBytes();
			DatagramPacket pacote = new DatagramPacket(dados, dados.length, cDestination, cPort);
			clientSocket.send(pacote);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getContainersLocalPort() throws IOException, InterruptedException {
		String[] command = { "/bin/bash", "-c", "docker ps | awk '{print $12}'"};
		ProcessBuilder processBuilder = new ProcessBuilder().command(command);
		ArrayList<String> dockerOutput = new ArrayList<String>();
		ArrayList<String> containerLocalPorts = new ArrayList<String>();
		try {
			Process process = processBuilder.start();

			// read the output
			InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String output = null;
			while ((output = bufferedReader.readLine()) != null) {
				dockerOutput.add(output);
			}

			// wait for the process to complete
			process.waitFor();

			// close the resources
			bufferedReader.close();
			process.destroy();

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		if (!dockerOutput.isEmpty()) {
			dockerOutput.remove(0);
			for (String i : dockerOutput) {
	            String linha=i.split("-")[0];
	            containerLocalPorts.add(linha.split(":")[1]);
			}	
		}
		return containerLocalPorts;
	}
}
